module cncp

go 1.13

replace github.com/ethereum/go-ethereum => github.com/ethereum/go-ethereum v1.9.11

require (
	git.cs.nctu.edu.tw/calee/sctp v1.1.0
	gitee.com/liu-zhao234568/amf v1.2.0
	gitee.com/liu-zhao234568/smf v1.0.0
	github.com/VictoriaMetrics/fastcache v1.6.0 // indirect
	github.com/btcsuite/btcd v0.20.1-beta // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/ethereum/go-ethereum v1.10.15 // indirect
	github.com/fjl/memsize v0.0.0-20190710130421-bcb5799ab5e5 // indirect
	github.com/free5gc/aper v1.0.1
	github.com/free5gc/ausf v1.0.0 // indirect
	github.com/free5gc/flowdesc v1.0.0 // indirect
	github.com/free5gc/fsm v1.0.0 // indirect
	github.com/free5gc/milenage v1.0.0 // indirect
	github.com/free5gc/nas v1.0.5 // indirect
	github.com/free5gc/ngap v1.0.2
	github.com/free5gc/openapi v1.0.3
	github.com/free5gc/pcf v1.0.0 // indirect
	github.com/free5gc/pfcp v1.0.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/google/uuid v1.1.5 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/golang-lru v0.5.5-0.20210104140557-80c98217689d // indirect
	github.com/huin/goupnp v1.0.2 // indirect
	github.com/karalabe/usb v0.0.0-20211005121534-4c5740d64559 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/prometheus/tsdb v0.7.1 // indirect
	github.com/rs/cors v1.7.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/syndtr/goleveldb v1.0.1-0.20210819022825-2ae1ddf74ef7 // indirect
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210816183151-1e6c022a8912 // indirect
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/olebedev/go-duktape.v3 v3.0.0-20200619000410-60c24ae608a6 // indirect
)
